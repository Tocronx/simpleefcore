﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Tocronx.SimpleEFCore.Sample.Models;

namespace Tocronx.SimpleEFCore.Sample
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("SimpleEFCore sample");

            var connectionString = "Data Source=" + "BlogDb.sqlite";
            var entityTypes = new Type[] { typeof(Blog), typeof(Post) };
            var queryTypes = new Type[] { typeof(PostContent) };
            SimpleDbContext.Init(entityTypes, queryTypes, x => x.UseSqlite(connectionString));

            using (var ctx = new SimpleDbContext())
            {
                ctx.EnsureCreated();

                var blog = new Blog() { Url = "https://Sample.blog" };
                ctx.Add(blog);
                ctx.Add(new Post() { Blog = blog, Title = "Entry 1", Content = "The content of entry 1." });
                ctx.Add(new Post() { Blog = blog, Title = "Entry 2", Content = "The content of entry 2." });
                ctx.Add(new Post() { Blog = blog, Title = "Entry 3", Content = "The content of entry 3." });

                ctx.SaveChanges();
            }

            using (var ctx = new SimpleDbContext(entityTypes, queryTypes, x => x.UseSqlite(connectionString)))
            {
                var blog = ctx.GetIQueryable<Blog>().Include(b => b.Posts).First(b => b.Posts.Any(p => p.Title == "Entry 1"));
                Console.WriteLine($"Post with title 'Entry 1' was found in blog '{blog.Url}'.");

                var postContent = ctx.FromSqlRaw<PostContent>($"SELECT Title, Content FROM Post WHERE Title LIKE '%2%'").First();
                Console.WriteLine($"Post with title containing '2' is '{postContent.Title}': '{postContent.Content}'.");
            }

            Console.ReadKey();
        }
    }
}
