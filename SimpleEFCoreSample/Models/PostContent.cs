﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tocronx.SimpleEFCore.Sample.Models
{
    public class PostContent
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
