﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tocronx.SimpleEFCore.Sample.Models
{
    public class Blog
    {
        public long BlogId { get; set; }
        public string Url { get; set; }
        public List<Post> Posts { get; set; }
    }
}
