﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tocronx.SimpleEFCore.Sample.Models
{
    public class Post
    {
        public long PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public long BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}
