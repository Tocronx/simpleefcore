﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tocronx.SimpleEFCore
{
    /// <summary>
    /// The IBaseDbContext interface provides methods for writing data to the database.
    /// </summary>
    public interface IBaseDbContext : IReadOnlyBaseDbContext, IDisposable
    {
        object Add(object entity);
        object Attach(object entity);
        object Update(object entity);
        object Remove(object entity);
        void AddRange(IEnumerable<object> entities);
        void AddRange(params object[] entities);
        void AttachRange(params object[] entities);
        void AttachRange(IEnumerable<object> entities);
        void RemoveRange(IEnumerable<object> entities);
        void RemoveRange(params object[] entities);
        void UpdateRange(params object[] entities);
        void UpdateRange(IEnumerable<object> entities);
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
