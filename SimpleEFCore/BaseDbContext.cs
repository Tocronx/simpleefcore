﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Tocronx.SimpleEFCore
{
    /// <summary>
    /// The BaseDbContext class implements basic functionality to simplify DbContext creation.
    /// </summary>
    public class BaseDbContext : DbContext, IBaseDbContext
    {
        protected readonly Dictionary<Type, object> typeToDbSetMapping = new Dictionary<Type, object>();
        protected readonly Type[] entityTypes;
        protected readonly Type[] queryTypes;

        public BaseDbContext(Type[] entityTypes, Type[] queryTypes, bool initializeDbSets = true)
        {
            this.entityTypes = entityTypes;
            this.queryTypes = queryTypes;
            if (initializeDbSets) { InitializeDbSets(); }
        }

        protected void InitializeDbSets()
        {
            MethodInfo setMethod = GetType().GetMethod(nameof(Set), Array.Empty<Type>())!;
            foreach (Type t in entityTypes.Concat(queryTypes))
            {
                try
                {
                    typeToDbSetMapping.Add(t, setMethod.MakeGenericMethod(t).Invoke(this, null)!);
                }
                catch (Exception e)
                {
                    Debug.WriteLine("BaseDbContext: " + e.Message);
                    throw;
                }
            }
        }


        public DbSet<T> GetDbSet<T>() where T : class
        {
            if (typeToDbSetMapping.TryGetValue(typeof(T), out object? dbSet))
            {
                return (DbSet<T>)dbSet;
            }
            var e = new KeyNotFoundException(GetType().Name + ": DbSet for type " + typeof(T).FullName + " not found!");
            Debug.WriteLine("BaseDbContext: " + e.Message);
            throw e;
        }

        public IQueryable<T> GetIQueryable<T>() where T : class
            => GetDbSet<T>();

        public IQueryable<T> FromSqlInterpolated<T>(FormattableString sql) where T : class
            => GetDbSet<T>().FromSqlInterpolated<T>(sql);

        public IQueryable<T> FromSqlRaw<T>(string sql) where T : class
            => GetDbSet<T>().FromSqlRaw<T>(sql);

        public IQueryable<T> FromSqlRaw<T>(string sql, params object[] parameters) where T : class
            => GetDbSet<T>().FromSqlRaw<T>(sql, parameters);

        public new object Add(object entity)
            => base.Add(entity);

        public new object Attach(object entity)
            => base.Attach(entity);

        public new object Update(object entity)
            => base.Update(entity);

        public new object Remove(object entity)
            => base.Remove(entity);

    }
}
