﻿using System;
using System.Linq;

namespace Tocronx.SimpleEFCore
{
    /// <summary>
    /// The IReadOnlyBaseDbContext interrface provides methods to read data from the database.
    /// </summary>
    public interface IReadOnlyBaseDbContext : IDisposable
    {
        IQueryable<T> GetIQueryable<T>() where T : class;
        IQueryable<T> FromSqlInterpolated<T>(FormattableString sql) where T : class;
        IQueryable<T> FromSqlRaw<T>(string sql) where T : class;
        IQueryable<T> FromSqlRaw<T>(string sql, params object[] parameters) where T : class;
    }
}
