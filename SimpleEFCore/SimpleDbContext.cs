﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;

namespace Tocronx.SimpleEFCore
{
    /// <summary>
    /// The SimpleDbContext provides a very simple and easily configurable DBContext, which is already sufficient for many use cases.
    /// </summary>
    public class SimpleDbContext : BaseDbContext
    {
        protected static Type[]? defaultEntityTypes;
        protected static Type[]? defaultQueryTypes;
        protected static Action<DbContextOptionsBuilder>? defaultOnConfiguringAction;

        protected Action<DbContextOptionsBuilder>? onConfiguringAction;

        public static void Init(Type[] entityTypes, Type[] queryTypes, Action<DbContextOptionsBuilder> onConfiguringAction)
        {
            defaultOnConfiguringAction = onConfiguringAction;
            defaultEntityTypes = entityTypes;
            defaultQueryTypes = queryTypes;
        }

        public SimpleDbContext() : this(defaultEntityTypes, defaultQueryTypes, defaultOnConfiguringAction ?? throw new Exception("Error initialize must be called first!")) { }

        public SimpleDbContext(Type[]? entityTypes, Type[]? queryTypes, Action<DbContextOptionsBuilder> onConfiguringAction)
            : base(entityTypes ?? Array.Empty<Type>(), queryTypes ?? Array.Empty<Type>(), false)
        {
            if (onConfiguringAction == null) { throw new ArgumentNullException(nameof(onConfiguringAction)); }
            this.onConfiguringAction = onConfiguringAction;
            InitializeDbSets();
        }

        public void EnsureCreated() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            onConfiguringAction?.Invoke(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var type in entityTypes ?? new Type[0])
            {
                try
                {
                    modelBuilder.Entity(type).ToTable(type.Name);
                }
                catch (Exception e)
                {
                    Debug.WriteLine("BaseDbContent: " + e.Message);
                    throw;
                }
            }
            foreach (var queryType in queryTypes ?? new Type[0])
            {
                try
                {
                    modelBuilder.Entity(queryType).HasNoKey();
                }
                catch (Exception e)
                {
                    Debug.WriteLine("BaseDbContent: " + e.Message);
                    throw;
                }
            }
        }

    }
}